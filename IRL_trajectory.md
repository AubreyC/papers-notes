# Inverse Reinforcement Learning for vehicle trajectories

## Chalenges:

- How to represent the state of the vehicle
- How to represent the environment:
    - Lane
    - Traffic signals
    - Other vehicles
- How to encode the goal of the agents:
    - Differement destinations

## State / Action space:

Survey on state / action space: https://hal.archives-ouvertes.fr/hal-01908175/

Challenges:

- Changing size
- Behavior should be the same for different location: ego-centric view is prefered
- Intention should be modeled somewhere

Representation:

- States:
    - Continous/Discrete coordinates: Changing size with the number of agents
    - Spatial grid representation: Size Indep of number of agents

- Actions:
    - Discrete continuous
    - Acceleration / Steering angle

[[Navigating Occluded Intersections with Autonomous Vehiclesusing Deep Reinforcement Learning][https://arxiv.org/pdf/1705.01196.pdf]]

## Latent state:

- Behavior cannot be explaned only by the obesrvation:
    - Actions fluenced by internal state: intented destiation, etc

- Explain successes iof Imitaiton Leaning simple on tasks such as lane following


[[End-to-end Driving via Conditional Imitation Learning][https://arxiv.org/pdf/1710.02410.pdf]]


### Example

- [[Learning from Demonstration in the Wild][https://arxiv.org/pdf/1811.03516.pdf]]
    - Observations:
        - Pseudo Lidar representation
        - Agent’s:
            - heading
            - distance
            - velocity
    - Actions:
        - Displacements in Cartesian coordinates

- [[Multi-Agent Imitation Learning for Driving Simulation][https://arxiv.org/pdf/1803.01044.pdf]]
    - Observations: (no full state)
        - Artificial Lidar view
        - Ego vehicle info: Position, velocity, heading, etc
        - Long/Lat accel
        - Indicators: Collision occuring, etc
        - Leading Vehicle: Info on vehicle in front if exist
    - Actions:
        - longitudinal acceleration and turnrate values as the vehicle action.

- [[Imitating Driver Behavior with Generative Adversarial Networks][https://arxiv.org/pdf/1701.06699.pdf]]
    - State:
        - Core features: Speed, Lane offset, Vehicle offset, etc
        - Lidar view
        - Indicator features: Collision, off track, etc

- [[ChauffeurNet:  Learning to Drive by Imitating the Best and Synthesizing the Worst][https://arxiv.org/pdf/1812.03079.pdf]]
    - State:
        - Encoded by images representing the informations:
            - Image of the roadmap
            - Image of Traffic light (stack of successives past states)

## Multi-Agent IRL:

- [[Learning from Demonstration in the Wild][https://arxiv.org/pdf/1811.03516.pdf]]
- [[Multi-Agent Generative Adversarial Imitation Learning][https://arxiv.org/abs/1807.09936]]
- [[Multi-Agent Imitation Learning for Driving Simulation][https://arxiv.org/pdf/1803.01044.pdf]]
- [[Imitating Driver Behavior with Generative Adversarial Networks][https://arxiv.org/pdf/1701.06699.pdf]]