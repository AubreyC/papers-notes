
## Multi-Agent IRL:

- [[Learning from Demonstration in the Wild][https://arxiv.org/pdf/1811.03516.pdf]]
- [[Multi-Agent Generative Adversarial Imitation Learning][https://arxiv.org/abs/1807.09936]]
- [[Multi-Agent Imitation Learning for Driving Simulation][https://arxiv.org/pdf/1803.01044.pdf]]
- [[Imitating Driver Behavior with Generative Adversarial Networks][https://arxiv.org/pdf/1701.06699.pdf]]

## Multi-Agent IRL:


- [[End-to-end  Driving  via  Conditional  Imitation  Learning][https://arxiv.org/pdf/1710.02410.pdf]]