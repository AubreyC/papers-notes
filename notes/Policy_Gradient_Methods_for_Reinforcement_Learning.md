# [Policy Gradient Methods for Reinforcement Learning with Function Approximation](https://dl.acm.org/citation.cfm?id=3009806)

## Context
- Use of function approximators for Reinforcement Learning
- Most of previous work/effort:
    - Focused on using function approximators to estimate a value function
    - Policy defined as the greedy policy: Action that maximizes the value function

## Problem
- Issue with the value function approach:
    - Oriented toward a deterministic policy but optimal policies usually stochastic
    - Very discontinuous as a small change in value-function can change the policy drastically
    - Convergence issues

## Idea / Solution / Method Proposed
- Instead of:
    - Function approximators to approximate value function
    - Deterministic policy from this approximate value function

- Approximate stochastic policy directly
    - Using a function approximator (e.g NN)
    - Policy Gradient based method to find the policy parameters

- Proof of some results:
    - Unbiased estimate of the policy gradient can be obtained from experience with an approximate value function (proved)
    - Given a policy parametrization, this results can be used to derive an appropriate form for the value-function parametrization (proved)
    - Using this results: A version of policy iteration with arbitrary differentiable function approximation converges to a locally optimal policy (proved)

## Limitation / Edge Cases

## Conclusion
- Theoretical results for Policy Gradient Method
- Allow to search for stochastic policy
- In this method, Policy can be parametrized in any way, as long as it is differentiable

## Summary

In the context of Reinforcement Learning, many methods rely on function approximators to approximate the value function. From this approximate, the policy is implicitely defined as the greedy policy, such that the action with the maximum value function associated is chosen. However, this approach defined an deterministic policy whereas optimal policy can be stochastic for some problems. Moreover, it defines a very discontinuous policy since a very small change in value function can induce a high change in the policy (an other action is chosen, changing the policy drastically).

This article proposes to directly approximate a stochastic policy with a function approximator parametrized by some parameters, such as Neural Network. Then the parameters are found using a policy gradient approach. This papers introduces some theoretical results for this method: An unbiased estimate of the policy gradient can be obtained from experience with an approximate value function. Then, using this result, a version of the policy iteration with arbitrary differentiable function approximation is proved to converge.

This article introduces very important results for Policy Gradient methods, allowing to search for optimal stochastic policy.

## Bibtex
```
@inproceedings{Sutton:1999:PGM:3009657.3009806,
 author = {Sutton, Richard S. and McAllester, David and Singh, Satinder and Mansour, Yishay},
 title = {Policy Gradient Methods for Reinforcement Learning with Function Approximation},
 booktitle = {Proceedings of the 12th International Conference on Neural Information Processing Systems},
 series = {NIPS'99},
 year = {1999},
 location = {Denver, CO},
 pages = {1057--1063},
 numpages = {7},
 url = {http://dl.acm.org/citation.cfm?id=3009657.3009806},
 acmid = {3009806},
 publisher = {MIT Press},
 address = {Cambridge, MA, USA},
}
```