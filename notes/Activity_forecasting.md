# [Activity Forecasting](http://repository.cmu.edu/cgi/viewcontent.cgi?article=1799&context=robotics)

## Context
- Activity forecasting:
    - Inferring future actions of agents
    - from noisy visual input

- Application to human trajectory forecasting

## Problem
- Vision-based activity analysis
    - Need to reason about the future: Goal
    - Require prior knowledge and goals

- Activity are driven by:
    - high level goals: e.g destinations
    - preferences with respect to physical scene features: e.g pedestrians prefer walking on sidewalks

## Idea / Solution / Method Proposed
- Leverage recent progress in:
    - Semantic scene labeling: used as feature to express preference (reward is express using these features)
    - Inverse Optimal Control / Inverse Reinforcement Learning

- Agent modeled as:
    - optimizing a cost function
    - cost function factors in the scene semantic (here linear combination of semantic features)

- Contribution
    - IOC to the field of vision-based activity analysis
    - hidden-variable MDP in context of IOC

- Hidden Variable Inverse Optimal Control
    - Different from traditional Inverse Optimal Control
        - states and actions not fully observable
    - Output:
        - recovering a distribution over a sequence of future actions
        - not a discrete activity categories

- Application
    - Forecasting distribution over all future trajectories
    - Destination forecasting from noisy observations: same method as in [*Maximum Entropy Inverse Reinforcement Learning*](B. Ziebart, 2008)
    - Smoothing
    - Knowledge transfer: Since cost function expressed as function of feature, it is not scene dependent

## Limitation / Edge Cases
- Cost function is linear
- Depends on Semantic scene labeling
- No real interaction between agents modeled but not really part of the scope of this work

## Conclusion
- Combination of semantic labeling and IOC is sound
- IOC algorithm from (B. Ziebart, 2008) can but computationally intensive / intractable

## Bibtex
```
@inproceedings{Kitani:2012:AF:2404742.2404759,
 author = {Kitani, Kris M. and Ziebart, Brian D. and Bagnell, James Andrew and Hebert, Martial},
 title = {Activity Forecasting},
 booktitle = {Proceedings of the 12th European Conference on Computer Vision - Volume Part IV},
 series = {ECCV'12},
 year = {2012},
 isbn = {978-3-642-33764-2},
 location = {Florence, Italy},
 pages = {201--214},
 numpages = {14},
 url = {http://dx.doi.org/10.1007/978-3-642-33765-9_15},
 doi = {10.1007/978-3-642-33765-9_15},
 acmid = {2404759},
 publisher = {Springer-Verlag},
 address = {Berlin, Heidelberg},
 keywords = {activity forecasting, inverse optimal control},
}
```