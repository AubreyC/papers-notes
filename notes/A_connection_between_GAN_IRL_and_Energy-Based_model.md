# [A connection between Generative Adversarial Networks, Inverse Reinforcement Learning and Energy-Based Models](https://arxiv.org/abs/1611.03852)

This paper derives a relationship between GAN and Maximum Entropy Inverse Reinforcement Learning (type of Energy Based model).
These notes are partial, and one may want to refer directly to the article.

Related to [*Generative Adversarial IMitation Learning* (J. Ho, 2016)](https://arxiv.org/abs/1606.03476)

## Context

- Generative Adversarial Network
- Learning from Demonstration with Inverse Reinforcement Learning
- Generative Adversarial Network
    - generator G: produce outputs output that will be classified by the discriminator as coming from the actual data distribution
    - discriminator D: aims at classifying inputs as either output of generator G or actual sample from data distribution p(x)

- Maximum Entropy IRL - special form of Energy Based Model (EBM):
    - Boltzmann Distribution
    - Energy is given by the cost function

## Problem

- Direct maximum Likelihood approach:
    - Simple approach to Imitation Learning / generative modeling:
        - train policy / generator to directly output distribution over the data
        - without learning discriminator or energy function

    - Generative model in multi-modal case when not expressive enough:
        - direct maximum likelihood results in distribution that covers all the modes (bad results)
        - instead training EBM, and then training generative model to be mode seeking results lower diversity but higher fidelity (more realistic)

    - In sequential decision-making problems
        - direct maximum likelihood is known as behavioral cloning
        - issue with compounding error

- Added value to learn the discriminator / Energy function

## Idea / Solution / Method Proposed

- GAN and IRL:
    - Underlying actual data distribution is assumed to be Boltzmann distribution with energy as the cost function (Maximum Entropy IRL formulation)
    - Optimal Discriminator is a combination of generator distribution and actual data distribution
        - uses known generator distribution to learn the optimal discriminator
        - which corresponds to learning the actual data distribution (and therefore the optimal cost function))

- GAN for training EBM:
    - In EBM estimating the partition function is very challenging
    - Adversarial training to estimate the partition function
        - generator produces samples with minimal energy
        - samples are used to estimate the partition function and therefore to get a better estimation of the energy function

## Conclusion
- With some small changes MaxEnt IRL and GAN optimization problem are shown to be equivalent
- Alternative approach for training energy based function using GAN

## Bibtex
```
@article{DBLP:journals/corr/FinnCAL16,
  author    = {Chelsea Finn and
               Paul F. Christiano and
               Pieter Abbeel and
               Sergey Levine},
  title     = {A Connection between Generative Adversarial Networks, Inverse Reinforcement
               Learning, and Energy-Based Models},
  journal   = {CoRR},
  volume    = {abs/1611.03852},
  year      = {2016},
  url       = {http://arxiv.org/abs/1611.03852},
  archivePrefix = {arXiv},
  eprint    = {1611.03852},
  timestamp = {Mon, 27 Nov 2017 09:23:01 +0100},
  biburl    = {https://dblp.org/rec/bib/journals/corr/FinnCAL16},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}
```