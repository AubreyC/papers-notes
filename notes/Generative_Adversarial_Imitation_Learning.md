# [Generative Adversarial Imitation Learning](https://arxiv.org/abs/1606.03476)

This paper introduces a framework based on GAN to directly learn the policy from the data, without having to recover the underlying cost function.
Related to [*A connection between Generative Adversarial Networks, Inverse Reinforcement Learning and Energy-Based Models* (C. Finn, 2017)](https://arxiv.org/abs/1611.03852)

One may want to refer direclty to the article as these notes are partial.

## Context
- Imitation Learning
- Learning to perform a task from expert demonstrations
    - Only provided with a finite set of demonstrations
    - Not allowed to query the expert for more data

## Problem
- Behavioral cloning:
    - Need a lot of data
    - Compounding error

- Inverse Reinforcement Learning
    - Expensive to run as it requires to run RL in the inner loop
    - learn the underlying cost function and need to be optimized to find the corresponding policy

## Idea / Solution / Method Proposed

- Duality between:
    - IRL and Occupency measure matching problem
    - running RL after IRL correspond to recovering the primal optimum (policy) from the dual optimum (cost function)

- Imitation learning method proposed
    - finds a policy whose occupency measure minimize the Jensen-Shannon divergence to the expert one
    - cost regulizer is defined as a maximum of a discriminator
    - Generative method where the goal is to find a saddle point (policy, discriminator)
        - minimize with the respect to the policy
        - maximize with respect to the discriminator

## Limitations
- Direclty get the policy and not the cost function which can be usefull as very succint

## Conclusion
- Duality between IRL and occupency measure learning
- Generative method where the goal is to find a saddle point (policy, discriminator)
    - minimize with the respect to the policy
    - maximize with respect to the discriminator

## Bibtex
```
@article{DBLP:journals/corr/HoE16,
  author    = {Jonathan Ho and
               Stefano Ermon},
  title     = {Generative Adversarial Imitation Learning},
  journal   = {CoRR},
  volume    = {abs/1606.03476},
  year      = {2016},
  url       = {http://arxiv.org/abs/1606.03476},
  archivePrefix = {arXiv},
  eprint    = {1606.03476},
  timestamp = {Wed, 07 Jun 2017 14:42:26 +0200},
  biburl    = {https://dblp.org/rec/bib/journals/corr/HoE16},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}
```