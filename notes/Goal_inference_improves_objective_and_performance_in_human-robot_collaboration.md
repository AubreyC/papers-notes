# [Goal Inference Improves Objective and Perceived Performance in Human-Robot Collaboration](https://arxiv.org/abs/1802.01780)

Human robot interaction, in a context of collaborative task. Robot infer immediate goal of the human, and optimize its own task taking the human goal prediction into account.

## Context
- Robots more and more present
- Collaborative is required in order to achieve good performance

## Problem
- In collaborative context:
    - Human and Robot need to collaborate to achieve a task

- Task allocation problem:
    - several task to accomplish
    - some are collaborative, some are individual

- In this paper:
    - Task are represented by being at a location
    - Task can be individual (only human or robot needs to go at this location)
    - Task can be collaborative (human and robot need to go at this location)

## Idea / Solution / Method Proposed

- Collaboration through inference of next goal for the human
    - Robot infer which task the human will try to achieve next
    - Robot plan accordingly in order to maximize efficiency (minimize time)

- Use Bayesian inference to get a posteriori estimate of the next goal of the human
- Result in anticipative robot collaboration


## Limitations
- Hard to test on real-human
- Human are not really rational agents (they do not always choose the optimal action)

## Conclusion
- Kind of a very simplified situation
- Interaction from the robot on the human is to taken into account:
    - only the robot adapt its behavior based on the prediction of what the human will do next

## Bibtex
```
@article{DBLP:journals/corr/abs-1802-01780,
  author    = {Chang Liu and
               Jessica B. Hamrick and
               Jaime F. Fisac and
               Anca D. Dragan and
               J. Karl Hedrick and
               S. Shankar Sastry and
               Thomas L. Griffiths},
  title     = {Goal Inference Improves Objective and Perceived Performance in Human-Robot
               Collaboration},
  journal   = {CoRR},
  volume    = {abs/1802.01780},
  year      = {2018},
  url       = {http://arxiv.org/abs/1802.01780},
  archivePrefix = {arXiv},
  eprint    = {1802.01780},
  timestamp = {Thu, 01 Mar 2018 15:00:45 +0100},
  biburl    = {https://dblp.org/rec/bib/journals/corr/abs-1802-01780},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}

```