# [Apprenticeship Learning for motion Planning with Application to Parking Lot Navigation](http://ieeexplore.ieee.org/document/4651222/)

## Context
- Motion and Path Planning
- Learn from demonstration: Demonstrated trajectories

## Problem
- Path planning:
    - Often use complex cost function carefully handcrafted
    - Much easier to demonstrate some trajectories

## Idea / Solution / Method Proposed

- Apprenticeship learning method:
    - Learn the weigh of the trade-offs between the different cost
    - e.g: cost function linear function of the features
    - Rely in max-margin formulation of the problem

- Apply Path planning algorithms with the learned cost function:
    - A-star algorithms on discrete coarse action space (sub-optimal since discrete action space)
    - Local smoothing using only a partial cost function (just to make is smoother)

## Limitation / Edge Cases
- Feature hand-crafted / Manually designed
- Cost function as linear combination of these features
- Predefined information: Driving lane of the environment
- Maximum Entropy IRL: Better since it does not favor any behavior from the demonstration
- No notion of velocity / Interaction with other agents

## Conclusion
- Interesting use-case of Max-Margin Inverse Reinforcement Learning algorithms
- Application on motion planning
- Since this article: Better algorithms e.g MaxEnt IRL

## Summary

This article presents an application case of the Apprenticeship Learning algorithm developed in "Apprenticeship Learning for Inverse Reinforcement Learning" (Abeel 2004)
In the context of motion and path planning, complex manually designed cost functions are usually used. However, it would be much easier to just demonstrate some good trajectories instead of manually hand-crafting these cost functions.

This articles introduces an algorithm that aims at learning the weights (e.g trade offs) between different cost (e.g features) from demonstrated trajectories. This can be seen as learning the linear coefficients of the cost function (assuming it is a linear combination of features). It uses an adaptation of the max-margin algorithms for Apprenticeship learning developed in "Apprenticeship Learning for Inverse Reinforcement Learning" (Abbeel 2004), based on Max-Margin approach. Once the weights have been learned, the path planning is done in two steps. First a A-star algorithm is used on a discretized (coarse) action space to get a rough path. Then a local smoother is applied on the path to get a smooth final planed path.

This articles is very interesting as it shows how to apply the Apprenticeship Learning algorithm on a motion / path planning problem. It show pretty good results on a parking lot navigraiton example. However, it does not take into account the velocity of the velocity (path and not trajectory), and assume that the cost function is a linear function of some hand-crafted features. Moreover, new / better methods have been developed since (e.g "Maximum Entropy Inverse Reinforcement Learning" B. Ziebart 2008).


## Bibtex
```
@INPROCEEDINGS{4651222,
author={P. Abbeel and D. Dolgov and A. Y. Ng and S. Thrun},
booktitle={2008 IEEE/RSJ International Conference on Intelligent Robots and Systems},
title={Apprenticeship learning for motion planning with application to parking lot navigation},
year={2008},
volume={},
number={},
pages={1083-1090},
keywords={automobiles;learning (artificial intelligence);mobile robots;motion control;navigation;path planning;position control;apprenticeship learning;complex cost function;global navigation;motion planning;parking lot navigation;path-planning algorithm;robotic car;trajectories smoothing;trajectory demonstration;Algorithm design and analysis;Distance measurement;Driver circuits;Navigation;Optimization;Robots;Trajectory},
doi={10.1109/IROS.2008.4651222},
ISSN={2153-0858},
month={Sept},}
```