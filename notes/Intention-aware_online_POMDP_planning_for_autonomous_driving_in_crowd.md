# [Intention-Aware Online POMDP Planning for Autonomous Driving in a Crowd](https://www.comp.nus.edu.sg/~leews/publications/bai2015intention.pdf)

Autonomous vehicle planning, path velocity decomposition. Path-planning with A-star, POMDP used for velocity planning.

## Context

- Autonomous vehicle trajectory planning
- Driving through crowded environment: pedestrian

## Problem

- Need to drive among pedestrian
    - safely
    - smoothly
    - efficiently

- Simple reactive methods do not work
    - only based on current system
    - do not capture intention / future motion
    - do not consider long term effect of actions

- Greedy decision making: e.g maximum likelihood
    - does not capture uncertainty
    - does not capture future effect of action on the informations gathered (e.g taking action to increase knowledge about situation)

- POMDP good framework
    - capture long term effect
    - capture uncertainty / partially observable state
    - untraceable in high dimension: Real-time performance issue

## Idea / Solution / Method Proposed

- Overall system: Decomposition path-velocity
    - Path planning: A* through less crowded region, with simple pedestrian motion model
    - POMDP planning: online resolution using DESPOT algo (allow for continuous space representation)
    - Belief of hidden state updated with Baye's rule with new observations

- POMDP:
    - States:
        - Car: position, orientation and speed
        - Pedestrian: position, goal (position) and velocity
    - Actions: Acceleration value (amplitude)
    - Steering of the car is handled by the path-planner A*

- Implementation on a golf car

## Limitations

- Path-velocity decomposition
    - Path might not be efficient at all
    - Path does not take/ very slightly interaction / intentions of pedestrian
    - Stop and Go effects

- Intention of pedestrian is very basic: just goal location (but doesn't mean it is not sound)

## Conclusion
- Application of POMDP to capture human intention
- Interesting applied test-case: How to apply it in the real world
- But decomposition path-velocity limits how the intentions of pedestrian are handled

## Bibtex
```
@INPROCEEDINGS{7139219,
author={H. Bai and S. Cai and N. Ye and D. Hsu and W. S. Lee},
booktitle={2015 IEEE International Conference on Robotics and Automation (ICRA)},
title={Intention-aware online POMDP planning for autonomous driving in a crowd},
year={2015},
volume={},
number={},
pages={454-460},
keywords={Markov processes;decision making;mobile robots;path planning;pedestrians;road vehicles;robust control;autonomous driving;autonomous vehicles;intention-aware online POMDP planning;partially observable Markov decision process;pedestrians safety;robot planning;robust decision making;Planning;Robot sensing systems;Uncertainty;Vegetation;Vehicle dynamics;Vehicles},
doi={10.1109/ICRA.2015.7139219},
ISSN={1050-4729},
month={May},}
```