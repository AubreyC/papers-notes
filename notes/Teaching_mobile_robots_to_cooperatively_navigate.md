# [Teaching  Mobile  Robots  to  Cooperatively  Navigate in  Populated  Environments](http://ais.informatik.uni-freiburg.de/publications/papers/kuderer13iros.pdf)

Based on *Feature-Based Prediction of Trajectories for Socially Compliant Navigation* (Kuderer, 2012)

## Context
- Mobile robot in human populated environment
- Cooperative navigation

## Problem
- Humans act in cooperative manner to avoid collisions
- Robots need to learn how human react to them in order to plan their trajectory accordingly

## Idea / Solution / Method Proposed
- Robot learn how to interact with human from demonstrations / examples

- Reasoning about homotopy classes that correspond to interaction classes

- Maximum entropy inverse reinforcement learning
    - behavior of the agents can be described by a probability distribution over composite trajectories
    - mode of probability distribution correspond to homotopy classes of interactions
    - use modes to approximate feature expectation (maximum likelihood)

- Only consider relevant homotopy class

- Planning
    - planning for all agents (composite trajectories)
    - taking the interaction with highest probability according to the learned distribution

## Limitations
- IRL algorithm:
    - computes expected feature values using Maximum Likelihood approximation for demonstrations
    - this might fail / be inaccurate for demonstrations that are sub-optimal due to stochasticity in human behavior
- Use feature and distribution parametrized by linear combination of these features
- Need to estimate the destination of surrounding agents in real time (used as goal)

## Conclusion
- Learning from demonstrations using with Maximum Entropy IRL principle
- Interesting approach to reason about homotopy classes
- Has some limitations

## Bibtex
```
@inproceedings{kuderer13iros,
  author = {Markus Kuderer and Henrik Kretzschmar and Wolfram Burgard},
  title = {Teaching Mobile Robots to Cooperatively Navigate in Populated Environments},
  booktitle = {Proc.~of the IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS)},
  year = 2013,
  address = {Tokyo, Japan},
  url = {http://www.informatik.uni-freiburg.de/~kretzsch/pdf/kuderer13iros.pdf},
}
```