# [Online Maneuver Recognition and Multimodal Trajectory Prediction for Intersection Assistance using Non-parametric Regression](https://pdfs.semanticscholar.org/3fde/bce9e1bac5ba22cd669e44ae01875b7524d9.pdf)

Gaussian Processes model to do maneuver recognition and trajectory prediction. The method uses a normalization procedure to be able to generalize the model to different intersections, and a particle filter using Gaussian Process regression model to to multi-modal trajectory predictions.
Related to [*Modelling of Traffic Situations at Urban Intersections with Probabilistic Non-parametric Regression*](https://pdfs.semanticscholar.org/83be/6609736dd4a85a72e30697dfa92b42ce5e33.pdf) (Q. Tran, 2013)

Interesting for the modeling of trajectories using Gaussian Processes. No interaction, and not very generalizable.

## Context
- ADAS system for urban intersections
    - Maneuver recognition
    - Trajectory prediction

## Problem
- Need a stochastic method to capture uncertainty and variability in trajectories
- Gaussian Processes are well suited to model trajectories as developed in prior work: *Modelling of Traffic Situations at Urban Intersections with Probabilistic Non-parametric Regression* (Q. Tran, 2013)
- Trajectory Prediction:
    - non linearities and multi-modalities
    - unimodal estimator (such as Kalman Filter) are not well suited

- Method need to:
    - work for different intersection without having to re-train the model
    - need to be able to perform multiple step ahead prediction

## Idea / Solution / Method Proposed
- Model trajectories using spatio-temporal Gaussian Processes model:
    - learn different regression model: turn right, turn left, go straight

- Feature normalization:
    - In a intersection centered frame
    - Spatial and temporal

- Identification by matching the most probable Gaussian Model to the current sequence of observations

- Prediction (not clear)
    - defined a mixed GP model based on previous regression model (not clear)
    - particle filter based on Gaussian Process regression model: At each time step, for each particle, this distribution is predicted based on the previous state and the provided GPR. 

- Data from the Kalrsruhe Institute of Technology
    - Data-point from intersection
    - Kalman Filter to smooth trajectories and get velocities

## Limitations
- Generalization is still pretty limited
    - intersection geometry need to be somehow similar, e.g regressions models (turn right, go straight, etc) need to be the same across the different intersections

- Seems to be limited to capture the global path
    - stop and go model as a separate regression model

- Does not capture the interaction between vehicles

## Conclusion
- Modeling trajectories with Guassian Processes is interesting
- Multi-modal prediction algorithm using particle filter and importance sampling based on learned Gaussian Processes is interesting
- But lack of generality and does not capture the interaction between vehicles
- Could be used:
    - for high level maneuver recognition
    - multi-modal trajectory prediction for prediction in an interaction model

## Bibtex
```
@inproceedings{DBLP:conf/ivs/TranF14,
  author    = {Quan Tran and
               Jonas Firl},
  title     = {Online maneuver recognition and multimodal trajectory prediction for
               intersection assistance using non-parametric regression},
  booktitle = {2014 {IEEE} Intelligent Vehicles Symposium Proceedings, Dearborn,
               MI, USA, June 8-11, 2014},
  pages     = {918--923},
  year      = {2014},
  crossref  = {DBLP:conf/ivs/2014},
  url       = {https://doi.org/10.1109/IVS.2014.6856480},
  doi       = {10.1109/IVS.2014.6856480},
  timestamp = {Fri, 26 May 2017 00:48:56 +0200},
  biburl    = {https://dblp.org/rec/bib/conf/ivs/TranF14},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}
```
