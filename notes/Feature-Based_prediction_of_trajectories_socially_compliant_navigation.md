# [Feature-Based Prediction of Trajectories for Socially Compliant Navigation](http://www.roboticsproceedings.org/rss08/p25.pdf)

Learn probability of joint trajectories based on trajectory samples, and then find the maximum likelihood to do prediction.


## Context
- Mobile robots operating in environment shared with humans
- Social behavior: Need to predict human behavior to plan their trajectory
- Recent studies suggest that human behavior are based on multiple criterion (time, avoiding collision, aggressiveness, etc)

## Problem
- Current approaches relies on defensive / reactive behavior
    - ignore interactions
    - predict human behavior
    - avoid conflict

## Idea / Solution / Method Proposed

- Propose method to learn human trajectories emerging from interaction
    - learn probability over joint trajectories
    - maximum likelihood of this probability to get joint trajectories predictions

- Based on Maximum Entropy Principle:
    - Learn the probability distribution over the possible joints trajectories (fitting observations)
    - Assume that agents (human) prefer trajectories with overall low costs
    - Does not assume underlying MDP, but consider continuous trajectories (so directly distribution over possible trajectories, and not a reward function)

- **Compute features expectation:**
    - sampled based approach
    - approximation taking dirac function at the modes of the distribution

- Method for on-line application
    - modes of probability correspond to different topological variants (crossing on one sides)
    - hierarchical optimization (adaptive discretization to speed up things)

## Limitations
- Works on probability distribution
- Interaction implicitly captured in the joint trajectory probability but not explicit

## Conclusion
- Application of Maximum Principle
- But directly works on probability distribution that might fail to generalize the idea

## Bibtex
```
@INPROCEEDINGS{kuderer12rss,
  author = {Markus Kuderer and Henrik Kretzschmar and Christoph Sprunk and Wolfram Burgard},
  title = {Feature-Based Prediction of Trajectories for Socially Compliant Navigation},
  booktitle = {Proc. of Robotics: Science and Systems (RSS)},
  year = 2012,
  month = jul,
  address = {Sydney, Australia}
}
```