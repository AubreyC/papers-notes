# [Deep Q-learning from Demonstrations](https://arxiv.org/abs/1704.03732)


Reinforcement Learning in situation where expert demonstrations are available. Expert demonstration are used to guide the algorithm toward the optimal policy by using a large margin loss to induce some imitation learning in the process.

Related to:

- Human-level control through deep reinforcement learning (Deep Mind, 2015) [[paper](https://storage.googleapis.com/deepmind-media/dqn/DQNNaturePaper.pdf)]
- Reinforcement Learning with Few Expert Demonstrations (A. Lakshminarayanan, 2016) [[paper](https://drive.google.com/file/d/0B4nMjK_Q9AcRUlQ5RU5nYnhhM2M/view)]

## Context

- Reinforcement Learning
- More efficient way to find optimal policy

## Problem

- Not efficient
    - Usually requires a lot of simulation during the training process

## Idea / Solution / Method Proposed

- Leverage Expert demonstration to speed up the learning process

- Combine Reinforcement Learning and Imitation Learning
    - Using transition samples from acting on the system
    - Using transition samples from expert demonstration

- Large margin loss to imitate the transition showed by the expert demonstrations

- Algorithm:
    - Pre-train policy using only expert demonstrations
        - Loss combining TD loss and Imitation Loss (large margin)
    - Algorithm interact with the system and add transition to the buffer
        - Sample transition from buffer (actor and expert transition with some weights associated)
        - Loss combining TD loss and Imitation Loss (large margin)

## Limitations

- How / When to use transition from expert and from acting?
- What if the expert is not really optimal for the specified reward ?
    - reward is hand-crafted
    - case where expert demonstrations come from human demonstrations

## Conclusion

- Main idea is to combine Reinforcement Learning and to guide the exploration using Imitation Learning

## Bibtex
```
@article{DBLP:journals/corr/HesterVPLSPSDOA17,
  author    = {Todd Hester and
               Matej Vecerik and
               Olivier Pietquin and
               Marc Lanctot and
               Tom Schaul and
               Bilal Piot and
               Andrew Sendonaris and
               Gabriel Dulac{-}Arnold and
               Ian Osband and
               John Agapiou and
               Joel Z. Leibo and
               Audrunas Gruslys},
  title     = {Learning from Demonstrations for Real World Reinforcement Learning},
  journal   = {CoRR},
  volume    = {abs/1704.03732},
  year      = {2017},
  url       = {http://arxiv.org/abs/1704.03732},
  archivePrefix = {arXiv},
  eprint    = {1704.03732},
  timestamp = {Wed, 07 Jun 2017 14:41:33 +0200},
  biburl    = {https://dblp.org/rec/bib/journals/corr/HesterVPLSPSDOA17},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}
```