# [Learning context sensitive behavior models from observations for predicting traffic situations](https://ieeexplore.ieee.org/document/6728484/)

Traffic modeling using models embedded in Dynamic Bayesian Network. Some models are learned and other are manually hand-defined.

## Context

- Advanced Driving Assistance System
- Estimating and Predicting traffic

## Problem

- Hard problem
    - continuous
    - highly dynamic
    - only partially observable
    - High interaction between agents
        - anticipation / influence of own action on other is essential

- Predicting traffic situations is only traceable because it shows a lot of regularities
    - motion according to kinematics
    - respect traffic rules
    - etc

- Need to define a model that captures these patterns / regularities

## Idea / Solution / Method Proposed

- Global architecture
    - Models learned
    - Relationship between models embedded in a Dynamic Bayesian Network

- Models are learned
    - from observations: using ramdom forests
    - using background knowledge

## Limitation / Edge Cases
- Everything is modeled as distribution probability
- Notions of goals is embedded in the lane following models
- Not sure how well it works on edge-case: e.g collision avoidance

## Conclusion
- Modeling interaction in a DBM is very interesting
- It allows to reduce the dimensionality compared to using MDP with states that captures everything
- Mix of learning and manually defined model is interesting: e.g dynamic of vehicle does not need to be learned

## Bibtex
```
@INPROCEEDINGS{6728484,
author={T. Gindele and S. Brechtel and R. Dillmann},
booktitle={16th International IEEE Conference on Intelligent Transportation Systems (ITSC 2013)},
title={Learning context sensitive behavior models from observations for predicting traffic situations},
year={2013},
volume={},
number={},
pages={1764-1771},
keywords={Bayes methods;behavioural sciences computing;belief networks;driver information systems;learning (artificial intelligence);road traffic;DBN;autonomous driving;continuous nonlinear context dependent process model learning;dynamic Bayesian network;hybrid state representation;probabilistic models;road network;sophisticated driver assistance systems;traffic participants;traffic participants behavior;traffic situation estimation;traffic situation prediction;vehicle dynamics;Atmospheric measurements;Bayes methods;Context;Particle measurements;Predictive models;Roads;Vehicles},
doi={10.1109/ITSC.2013.6728484},
ISSN={2153-0009},
month={Oct},}
```