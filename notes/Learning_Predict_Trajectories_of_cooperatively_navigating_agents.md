# [Learning to Predict Trajectories of Cooperatively Navigating Agents](http://www2.informatik.uni-freiburg.de/~kretzsch/pdf/kretzschmar14icra.pdf)

Method to learn navigation behavior of interacting agents from demonstrations, modeled as mixture distributions (discrete over homotopy classes and continuous inside homotopy classes).
*Feature-Based Prediction of Trajectories for Socially Compliant Navigation*(M. Kuderer, 2012)

## Context
- Robot in human environment
- Modeling navigation behavior of multiple interacting agents (walking agents here)

## Problem
- Navigation behavior is not deterministic
- Cooperative and Interacting behavior

## Idea / Solution / Method Proposed
- Navigation behavior is stochastic and not deterministic: modeled as mixture probability distribution

- Model navigation behavior using mixture probability distributions over
    - discrete homotopy classes: modeling discrete decision
    - continuous inside homotopy classes: model physical behavior (velocity, etc)

- Use Maximum Entropy principle for the behavior distribution
    - based on trajectory demonstrations
    - using hand-crafted features

- Feature expectation over continuous trajectories of high dimensionality
    - based on Monte Carlo sampling
    - uses Hybrid Monte Carlo algorithm (refer to the article)

## Limitations
- Distribution parametrized by linear combination of hand-crafter features
- Distribution and not cost function (but it seems to be equivalent in this formulation)

## Conclusion
- Probability distribution over the trajectory of all the agents
- Really close to MaxEnt IRL (distribution actually optimize underlying cost function)
- Interesting cascaded approach:
    - mixture distribution
    - discrete decision probability distribution and continuous one for actual trajectory
- Actual experiment yields good results

## Bibtex
```
@inproceedings{kretzschmar14icra,
  author = {Henrik Kretzschmar and Markus Kuderer and Wolfram Burgard},
  title = {Learning to Predict Trajectories of Cooperatively Navigating Agents},
  booktitle = {Proc.~of the IEEE International Conference on Robotics and Automation (ICRA)},
  year = 2014,
  address = {Hong Kong, China},
  url = {http://www.informatik.uni-freiburg.de/~kretzsch/pdf/kretzschmar14icra.pdf},
}
```