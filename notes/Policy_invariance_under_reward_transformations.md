# [Policy invariance under reward transformations: Theory and application to reward shaping.](https://people.eecs.berkeley.edu/~russell/papers/ml99-shaping.ps)

## Context
- Reinforcement Learning
- Reward function is the task-description
- Policy invariance:
    - Several reward can lead to the same optimal policy

## Problem

- Some reward make it easier to compute the optimal policy
    - reward "lead" toward the optimal policy
    - ex: reward with only high reward at the end is very difficult to optimize

- Sometimes, when manually designing reward
    - optimal policy does not do what it was supposed to do
    - ex: weird behavior that would maximize reward, but not do what we wanted

- Very Useful:
    - Structural estimation: IRL, etc
    - Reward shaping: supply additional reward to guide the agent in the learning process

## Idea / Solution / Method Proposed

- Explore policy invariance
    - find reward function that would help the learning process
    - would lead to the same optimal policy as the initial MDP

- Difference of potential based reward shaping:
    - If no prior knowledge on transition and reward
    - Necessary and Sufficient condition for policy invariance

- Enable to speed up the learning process

## Conclusion
- Formal definition of policy invariance
- Very useful in the context of Inverse Reinforcement Learning

## Bibtex
```
@inproceedings{Ng:1999:PIU:645528.657613,
 author = {Ng, Andrew Y. and Harada, Daishi and Russell, Stuart J.},
 title = {Policy Invariance Under Reward Transformations: Theory and Application to Reward Shaping},
 booktitle = {Proceedings of the Sixteenth International Conference on Machine Learning},
 series = {ICML '99},
 year = {1999},
 isbn = {1-55860-612-2},
 pages = {278--287},
 numpages = {10},
 url = {http://dl.acm.org/citation.cfm?id=645528.657613},
 acmid = {657613},
 publisher = {Morgan Kaufmann Publishers Inc.},
 address = {San Francisco, CA, USA},
}
```