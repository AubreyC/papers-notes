# [Boosting Structured Prediction for Imitation Learning](https://papers.nips.cc/paper/3154-boosting-structured-prediction-for-imitation-learning.pdf)

Based on "Maximum margin planning" (Ratliff et al., 2006), but adding boosting such that the algorithm can learn to combine features in a non-linear way to learn more complex bahavior.

## Context
- Easier to show desired behavior than to manually design cost functions that induce this behavior

- Imitation Learning:
    - Learn linear mapping from features to cost functions in planning domain
    - Learn policy: Planning by minimiziing the learn costs functio

- Mapping / Learned cost function:
    - Such that demonstrated behavior (from teacher) appear lower cost

## Problem
- Formulation assumes the cost map is a linear function of a set of prespecified features
- Hard to find / handcraft these features

## Idea / Solution / Method Proposed
- Use a boosting procedure for learning a nonlinear function of the base features (from "Functional gradient techniques for combining hypotheses." Mason 1999)

- Gradient boosting framework: "Error limiting reductions between classification tasks"(Beygelzimer et al., 2005)
    - Reduce finding good features for structured prediction to a problem of simple classification.

- Learn new feature by learning a classifier that is best correlated with the changes we would like to have made to locally decrease the loss

## Limitation / Edge Cases
- Applied on Path (no velocity information in the example)
- Rely on Maximum Margin Planning but better algorithms sicne (MaxEnt IRL) such that some trajectories from examples are not prefered over other

## Conclusion
- Extend work from Maximum Margin Planning
- Rely on classification algorithm to compute non-linear combination of features

## Bibtex
```
@conference{Ratliff-2007-17031,
author = {Nathan Ratliff and David Bradley and J. Andrew (Drew) Bagnell and Joel Chestnutt},
title = {Boosting Structured Prediction for Imitation Learning},
booktitle = {Advances in Neural Information Processing Systems 19},
year = {2007},
month = {January},
editor = {B. Sch"{o}lkopf and J.C. Platt and T. Hofmann"},
publisher = {MIT Press},
address = {Cambridge, MA},
keywords = {Structured Prediction, Boosting, imitation learning, robotics, Margin},
}
```