# [Enabling Robots to Communicate their Objectives](https://arxiv.org/abs/1702.03465)

Robots that model how people infer their objective function from observations, and select behavior that are maximally informative.
Contribution from the user study, not really the theoretical.
Related to [*Robot Planning with mathematical Models of Human State and Action* (A. Dragan, 2017)](https://arxiv.org/abs/1705.04226)

## Context
- Human need to be able to anticipate / predict robot actions
- Learning from Demonstrations
- Teaching behavior: behavior that are maximally informative about the underlying objective function

## Problem

- Human need to understand robots to be able to predict / not be surprised by their actions

- People making inference about the robot objective
    - might not able to asses if a demonstration is optimal or not
    - uncertainty in the learning process


## Idea / Solution / Method Proposed

- Algorithmic Teaching Framework
    - Selecting environments (action, situation, etc)
    - Maximally informative based on the inference model

- Inference model: How human infer objective function from demonstrations
    - Exact Inference: Trajectories are perfectly optimal
    - Approximate Inference:
        - Deterministic: threshold on distance between optimal trajectory (with respect to reward param) to the observed trajectory
        - Probabilistic effect: Decrease probability with the distance to optimal trajectory (with respect to reward param)
            - Same as MaxEnt IRl just different point of view: here uncertainty is on the 


- Contribution
    - Different approximate inference models
    - User study to compare the difference inference models

## Limitation
- User study can be biased
- Inference models are straightforward

## Conclusion
- Idea of behavior to be maximally informative is interesting
- Learner has uncertainty instead of the demonstrator: Usually the inverse scenario is considered (MaxEnt IRL)

## Bibtex
```
@article{DBLP:journals/corr/HuangHAD17,
  author    = {Sandy H. Huang and
               David Held and
               Pieter Abbeel and
               Anca D. Dragan},
  title     = {Enabling Robots to Communicate their Objectives},
  journal   = {CoRR},
  volume    = {abs/1702.03465},
  year      = {2017},
  url       = {http://arxiv.org/abs/1702.03465},
  archivePrefix = {arXiv},
  eprint    = {1702.03465},
  timestamp = {Wed, 07 Jun 2017 14:40:28 +0200},
  biburl    = {https://dblp.org/rec/bib/journals/corr/HuangHAD17},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}
```