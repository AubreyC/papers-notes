# [Planning for autonomous car that leverage effects on human actions](https://people.eecs.berkeley.edu/~sseshia/pubdir/rss16.pdf)

This method takes into account the effect of autonomous car actions on human driver, and plans trajectory for autonomous car accordingly.

## Context
- Trajectory Planning for autonomous car
- Interactions between human and self-driving cars
- Actions of self-driving car influence human drivers

## Problem
- Current autonomous cars:
    - make predictions about other vehicles
    - plan their trajectory to stay out of their way
- But autonomous car's actions influence the actions of other vehicles
- Leverage response of other agents to plan more efficient trajectory

## Idea / Solution / Method Proposed
- Interaction between human and autonomous car modeled as a dynamical system:
    - autonomous cars maximize a reward
    - humans maximize a reward function

- Human drivers:
    - modeled as an optimal controller that optimize a reward function
    - learned by using Inverse Reinforcement Learning (IRL)

- Autonomous car planner:
    - optimizes the dynamical system which captures autonomous and human car
    - the dynamical system models human behavior through a reward function learned with IRL

## Limitations
- Assume that the human driver will comply to the autonomous car trajectory / decision

- Robustness to human modeling error?
    - what if the human reward used as a human model is wrong
    - autonomous car reward should always emphasize on defensive / safe behaviors

- all humans cannot be modeled by the same reward

- not clear how to scale up to more than two vehicles

## Conclusion
- taking advantage of interaction:
    - humans react to autonomous car actions
    - allows to achieve more efficient/natural behavior

- really interesting approach combining:
    - IRL for human behavior modeling
    - planning taking into account interaction between human and autonomous car

- limited to simple situations:
    - well defined roads / intersections
    - only two agents: autonomous and human car


## Bibtex
```
@inproceedings{sadigh-rss16,
  author    = {Dorsa Sadigh and Shankar Sastry and Sanjit A. Seshia and Anca D. Dragan},
  title     = {Planning for Autonomous Cars that Leverages Effects on Human Actions},
 booktitle = {Proceedings of the Robotics: Science and Systems Conference (RSS)},
 month = "June",
 year = {2016},
 OPTpages = {66--73},
 abstract = {},
}
```