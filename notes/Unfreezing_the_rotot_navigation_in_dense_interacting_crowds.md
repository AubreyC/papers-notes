# [Unfreezing the Robot: Navigation in Dense, Interacting Crowds](https://las.inf.ethz.ch/files/trautman10unfreezing.pdf)

Path planning using Gaussian Processes, with Interactions modeled by coupling Gaussian Processes using interaction potentials. Prediction and navigation is done by approximate inference.

## Context
- Mobile robots in crowd of dynamics agents
- Planning trajectories that take into account interaction between agents

## Problem
- Freezing Robot Problem:
    - robot believes environment is unsafe
    - every path is expected to collide with an agent due to massive uncertainty

- Current algorithms either:
    - static of ignore dynamic uncertainty
    - ignore interaction and high uncertainty leads to freezing (IRL to model human behavior, but as independent agent ref: "Planning-based prediction for pedestrian"  B. Ziebart 2009)

## Idea / Solution / Method Proposed
- Model trajectory as Gaussian Processes:
    - waypoints or goal information incorporated as measurement with low variances in the GP

- Model interaction in Gaussian Processes:
    - dynamic interactions captured as dependencies between GPs
    - interaction potential (model joint collision avoidance)

- Planning and prediction:
    - uses Maximum a Posteriori estimation (MAP, inference)
    - importance sampling based approach for approximate solution as not Gaussian anymore (since added interaction coupling)

## Limitations
- Not clear how the prior distribution is learned from data
- Motion model of humans captured in the prior from data ?
- Seems hard to encode other type of information than just learning prior from data

## Conclusion
- Interesting Gaussian process approach modeling interaction
- Tested in real data for prediction, it yielded good results

- Other related approach:
    -"Predicting actions to act predictably: cooperative partial motion planning with maximum Entropy models" (M. Pfeiffer 2016)
    - "Social LSTM: Human Trajectory Prediction in crowded space" (A. Alahi)

## Bibtex
```
@INPROCEEDINGS{5654369,
author={P. Trautman and A. Krause},
booktitle={2010 IEEE/RSJ International Conference on Intelligent Robots and Systems},
title={Unfreezing the robot: Navigation in dense, interacting crowds},
year={2010},
volume={},
number={},
pages={797-803},
keywords={Gaussian processes;collision avoidance;mobile robots;robot vision;uncertain systems;IGP;collision avoidance;crowd interaction estimation;freezing robot problem;goal-driven navigation;human pedestrian;interacting Gaussian process;mobile robot;nonparametric statistical model;predictive covariance;robot navigation;safe navigation;uncertain trajectory},
doi={10.1109/IROS.2010.5654369},
ISSN={2153-0858},
month={Oct},}
```