# [Algorithms for Inverse Reinforcement Learning](http://martin.zinkevich.org/publications/maximummarginplanning.pdf)

## Context
- Imitation Learning
- Learn mapping from feature to cost function so that an optimal policy with these costs would mimic the expert behavior
- Not necessarily only one MDP for the expert behavior

## Problem
- Hand-crafting reward/cost function is hard
- Easier to demonstrate the task

- Differs from IRL as:
    - MMP designed to allow demonstration from several MDP
    - MMP aims at extrapolating an entirely new maps and gaol

- MMP aims at:
    - Directly mimic output behavior
    - Agnostic about the real underlying MDP or true reward function

## Idea / Solution / Method Proposed
- Method to automate the mapping from features to cost
- Optimal policy with the learned cost function imitates the expert's behavior

- Reward as linear function of features:
    - Optimize / find reward weights such that the best policy according to the learned reward is close to the demonstrated one
    - Maximum margin optimization framework brings robustness to the solution as it makes the chosen solution significantly better than the others

- Proposed a new approach to solve the optimization problem
    - Based on sub-gradient method

- Planning done by finding the policy that optimize the learned reward

## Limitation / Edge Cases
- In this framework, a set of training instance is given
    - Correspond to examples from different MDPs (?)
    - Reward weight (reward linear function of features) are learned
    - Not clear on which MDP this reward function is applied

- For new example: What MDP are we in?

## Conclusion

- Maximum Margin Approach interesting as it brings robustness to the problem
- Quadratic Programming formulation / Efficient optimization method introduced using sub-gradient methods
- Ignore dynamic of underlying MDP
- Aims at finding a reward that would mimic the demonstrated behavior, without ensuring that's the real underlying one or not
- General approach, a little bit hard to understand

## Summary

Imitation Learning aims at learning how to perform a task from demonstrations. In an MDP framework, it usually aims at recovering the underlying reward/cost function from a set of expert demonstrations. Then, the optimal policy can be obtained by optimizing this reward function on the MDP. This article introduces a method to learn a mapping from features to cost, so that an optimal policy with these costs would mimic the expert behaviors.

The algorithm relies on demonstrations from several different MDP (process and reward functions). It differs from IRL as it does not aim at recovering the true underlying reward function (as there might be many of it), but at constructing a reward function such that the best policy according to the learned reward is close to the demonstrated behaviors. This algorithm aims at directly mimicking output behavior, and is agnostic about the real underlying MDPs. It is design to extrapolate an entirely new map and goal from different behaviors. The problem is formulated using the Maximum margin optimization framework, which brings robustness as it makes the chosen solution significantly better than the others. Furthermore, an sub-gradient based optimization approach is proposed which enables to simplify the solving of the maximum margin problem.

Interesting approach, as Maximum Margin Approach brings robustness to the solution. It has been re-used in "Apprenticeship Learning via Inverse Reinforcement Learning" (Abbeel), more in a IRL approach. It is a little bit confusing as the problem aims at computing a reward that would make policy optimal across several different MDPs, so the learned reward is not the true one, but just one that would mimic the expert's behavior.

## Bibtex
```
@inproceedings{Ratliff:2006:MMP:1143844.1143936,
 author = {Ratliff, Nathan D. and Bagnell, J. Andrew and Zinkevich, Martin A.},
 title = {Maximum Margin Planning},
 booktitle = {Proceedings of the 23rd International Conference on Machine Learning},
 series = {ICML '06},
 year = {2006},
 isbn = {1-59593-383-2},
 location = {Pittsburgh, Pennsylvania, USA},
 pages = {729--736},
 numpages = {8},
 url = {http://doi.acm.org/10.1145/1143844.1143936},
 doi = {10.1145/1143844.1143936},
 acmid = {1143936},
 publisher = {ACM},
 address = {New York, NY, USA},
}
```