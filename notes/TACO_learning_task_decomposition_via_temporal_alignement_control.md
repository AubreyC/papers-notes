# [TACO: Learning Task Decomposition via Temporal Alignment Control](https://arxiv.org/abs/1803.01840)

Learning from Demonstration - Behavior Cloning (directly learn policies)
Decomposition of a task in sub-tasks (reusable / generalization)
Sequence Alignment and policies learning at the same time

## Context
- Learning from Demonstration
- Decomposition of simple sub-tasks for
    - generalization
    - reutilization of learned sub-tasks
- Behavior Cloning
    - Directly learning policy from examples
    - Not rely on an underlying cost function

## Problem
- Real world tasks are often very complex
    - Learning one single monolithic policy for the whole task
    - Result in complex and less reusable policy

- Modular Learning from Demonstration
    - task is modeled as a composition of sub-tasks
    - reusable sub-policy are learn for each sub-tasks
    - sub-tasks can be rearrange to execute new tasks

- Training requires additional information
    - labor intensive
    - sub-optimal if additional information is incorrect/inaccurate

## Idea / Solution / Method Proposed
- Modular Learning from Demonstration from
    - examples
    - task sketch sub-tasks (ordered) performed om the example set but not alignment (action-state to sub-tasks correspondence)

- Sequence Alignment:
    - Associate state/actions pairs to a sub-tasks
    - Connectionist Temporal Classification (CTC) (A. Graves, 2016)

- Temporal Alignment for Control
    - Alignment and Policies learning  (for each sub-tasks) is done simultaneously

## Limitations
- Learn policy directly
    - less generalizable than learning underlying cost-function
- Might be interesting to apply this modular approach to IRL

## Conclusion
- Interesting approach
- Yields goo results on benchmark test

## Bibtex
```
@ARTICLE{2018arXiv180301840S,
   author = {{Shiarlis}, K. and {Wulfmeier}, M. and {Salter}, S. and {Whiteson}, S. and
    {Posner}, I.},
    title = "{TACO: Learning Task Decomposition via Temporal Alignment for Control}",
  journal = {ArXiv e-prints},
archivePrefix = "arXiv",
   eprint = {1803.01840},
 primaryClass = "cs.LG",
 keywords = {Computer Science - Learning, Statistics - Machine Learning},
     year = 2018,
    month = mar,
   adsurl = {http://adsabs.harvard.edu/abs/2018arXiv180301840S},
  adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
```