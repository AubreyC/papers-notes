# [Learning Driving Styles for Autonomous Vehicles from Demonstrations](http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=7139555)

Method to learn driving style from human demonstrations, modeled as a reward function (linear combination of features) learned through inverse reinforcement learning.

Close to: *Planning for Autonomous Cars that Leverages Effects on Human Actions* (D. Sadigh, 2016)

## Context
- Learning driving style from human demonstrations

## Problem
- Large number of parameters involves in driving style (e.g distance to other, acceleration profile, etc)
- Hard to manually hand-tune

## Idea / Solution / Method Proposed
- Driving style modeled as a reward function that the human is trying to optimize
    - Expressed as linear combination of features
    - Features capture important properties of the situation

- Consider continuous state and action space to capture higher order
 properties such as jerk
    - trajectories are represented using quintic splines segments (each segment has 6 degrees of freedom that defines completely the motion)

- Inverse Reinforcement Learning:
    - **expected feature values computed using maximum likelihood approximation**
    - **effectively assumes that demonstrations are obtained by minimizing a cost function deterministically**

- Planning
    - uses the learned reward function to plan trajectory
    - assumes constant velocity model for surrounding vehicles

## Limitations
- IRL algorithm:
    - computes expected feature values using Maximum Likelihood approximation for demonstrations
    - this might fail / be inaccurate for demonstrations that are sub-optimal due to stochasticity in human behavior

- Linear cost function:
    - do not capture non-linearity

- Planing
    - assume constant velocity model for surrounding vehicles, might be inaccurate
    - ignore interaction / coupling in behavior

## Conclusion
- Using IRL to learn driver behavior (not new though)
- Continuous trajectories by using splines is interesting idea
- some limitations

## Bibtex
```
@INPROCEEDINGS{7139555,
author={M. Kuderer and S. Gulati and W. Burgard},
booktitle={2015 IEEE International Conference on Robotics and Automation (ICRA)},
title={Learning driving styles for autonomous vehicles from demonstration},
year={2015},
volume={},
number={},
pages={2641-2646},
keywords={learning systems;mobile robots;acceleration profiles;autonomous vehicles;cost function;demonstration approach;feature-based inverse reinforcement learning;learning driving styles;Acceleration;Cost function;Mobile robots;Navigation;Road transportation;Trajectory;Vehicles},
doi={10.1109/ICRA.2015.7139555},
ISSN={1050-4729},
month={May},}
```