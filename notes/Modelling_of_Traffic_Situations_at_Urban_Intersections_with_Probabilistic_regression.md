# [Modelling of Traffic Situations at Urban Intersections with Probabilistic Non-parametric Regression](https://pdfs.semanticscholar.org/83be/6609736dd4a85a72e30697dfa92b42ce5e33.pdf)

Related to [*Online Maneuver Recognition and Multimodal Trajectory Prediction for Intersection Assistance using Non-parametric Regression*](https://pdfs.semanticscholar.org/83be/https://pdfs.semanticscholar.org/3fde/bce9e1bac5ba22cd669e44ae01875b7524d9.pdf.pdf) (Q. Tran, 2014)

Interesting for the modeling of trajectories using Gaussian Processes. No interaction, and not very generalizable.

## Context
- ADAS system for urban intersections
    - Maneuver recognition
    - Trajectory prediction

## Problem
- Need a stochastic method to capture uncertainty and variability in trajectories
- Gaussian Processes are well suited to model trajectories as developed in prior work: *Modelling of Traffic Situations at Urban Intersections with Probabilistic Non-parametric Regression* (Q. Tran, 2013)
- Trajectory Prediction:
    - non linearities and multi-modalities
    - unimodal estimator (such as Kalman Filter) are not well suited

## Idea / Solution / Method Proposed
- Model trajectories using spatio-temporal Gaussian Processes model:
    - Learn different regression model: turn right, turn left, go straight

- Identification by matching the most probable Gaussian Model to the current sequence of observations

- Prediction
    - Using UKF to handle non-linearity
    - GP is used to generate the sigma particules that are propagated trhough the filter

- Data from the Kalrsruhe Institute of Technology
    - Data-point from intersection
    - Kalman Filter to smooth trajectories and get velocities

## Limitations
- Generalization is still pretty limited
- Seems to be limited to capture the global path
- Does not capture the interaction between vehicles

## Conclusion
- Modeling trajectories with Guassian Processes is interesting
- But lack of generality and does not capture the interaction between vehicles

## Bibtex
```
@INPROCEEDINGS{6629491,
author={Q. Tran and J. Firl},
booktitle={2013 IEEE Intelligent Vehicles Symposium (IV)},
title={Modelling of traffic situations at urban intersections with probabilistic non-parametric regression},
year={2013},
volume={},
number={},
pages={334-339},
keywords={Gaussian processes;Kalman filters;driver information systems;pattern recognition;regression analysis;ADAS;UKF;advanced driver assistance systems;driving intention recognition;moving vehicle trajectory prediction;probabilistic nonparametric regression;real traffic scenarios;spatio-temporal dependencies;traffic situation modelling;two-dimensional Gaussian process regression;unscented Kalman filters;urban intersections;Gaussian processes;Hidden Markov models;Kalman filters;Predictive models;Probabilistic logic;Trajectory;Vehicles;Gaussian process regression;Intersection assistance;Un-scented Kalman filter;driver intention recognition;trajectory prediction},
doi={10.1109/IVS.2013.6629491},
ISSN={1931-0587},
month={June},}
```
