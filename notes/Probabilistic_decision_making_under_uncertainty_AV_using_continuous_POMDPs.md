# [Probabilistic Decision-Making under Uncertainty for Autonomous Driving using Continuous POMDPs](https://ieeexplore.ieee.org/document/6957722/)

System is modeled as a continuous POMDP with all the vehicles as part of the system. POMDP is solved with a continuous solver that adapts the discretization for the current state.

## Context
- Decision making with other agents
- Uncertainty due to occlusion and sensors noise

## Problem
- Decision problem is hard:
    - noisy perception: sensors
    - incomplete knowledge of the situation: occlusion, other agents' behavior

- Discrete POMDP
    - Optimal discretization is not the same depending on the situation
    - Discretization influences performance: computation / optimality

## Idea / Solution / Method Proposed

- System is modeled as a continuous POMDP:
    - Accounts for other agents
    - Incorporates uncertainty
    - Avoid discretization issues

- Solve POMDPs using continuous solver: *Solving Continuous POMDPs:  Value Iteration with Incremental Learning of an Efficient Space Representation* (S. Bretchel, 2013)
    - Adapt discretization to the current state

- Other vehicles actions / behavior are modeled: *Learning Context Sensitive Behavior Models from Observations for Predicting Traffic Situations* (T. Gindel, 2013)

## Limitation / Edge Cases
- Account for interaction between vehicles as other agents are in the model but it is a little bit confusing on how exactly that works
- Only Ego vehicle adapts its behavior to other agents
- Reward function defining the ego-vehicle behavior is hand-crafted, therefore pretty simplistic

## Conclusion
- Modeling system with continuous POMDPs is interesting
- Only simulated results which doesn't capture real interaction
- Issue to solve it in real-time
- Parametrization depends of the situation

## Bibtex
```
@INPROCEEDINGS{6957722,
author={S. Brechtel and T. Gindele and R. Dillmann},
booktitle={17th International IEEE Conference on Intelligent Transportation Systems (ITSC)},
title={Probabilistic decision-making under uncertainty for autonomous driving using continuous POMDPs},
year={2014},
volume={},
number={},
pages={392-399},
keywords={Markov processes;automobiles;decision making;decision theory;mobile robots;probability;autonomous driving;belief space;continuous POMDP solver;continuous POMDPs;continuous partially observable Markov decision process;continuous-space problem;infinite-dimensional;probabilistic decision-making;rational decision-making;tactical decision-making;temporal evolution;Bayes methods;Complexity theory;Decision making;Roads;Sensors;Uncertainty;Vehicles},
doi={10.1109/ITSC.2014.6957722},
ISSN={2153-0009},
month={Oct},}
```