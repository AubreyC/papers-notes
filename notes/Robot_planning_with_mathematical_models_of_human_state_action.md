# [Robot Planning with mathematical Models of Human State and Action](https://arxiv.org/abs/1705.04226)

Summary of work in integrating computational cognitive models of people into robotics planning and control.
One may want to refer directly to the article since these are only partial / personal notes.
Plenty of very interesting references of related work (with more details).

## Context
- Robots interacting withs physical world / humans
- Interaction formulation of robotics problems

## Problem
- Robot have to interact with humans
    - Need to make prediction about future actions
    - Need to make prediction about goals

- Prediction helps coordination / avoid conflict

- Formulate the interaction as a 2 players game
    - Each player tries to maximize its own reward
    - Rationally solving this game
        - Computationally intractable
        - Many equilibrium if different rewards
        - Not really what we do: we adapt behavior / reward to situations

## Idea / Solution / Method Proposed

- System global
    - Human and Robot have reward functions

### Human Behavior during interaction

- Perfect collaboration
    - Same reward functions known by the agents
    - Optimize the global system
        - Each time-step (re-planning enabling to constantly adapt to the human behavior / reactive planning)
        - Apply first actions of the output actions sequence
        - Robot assumes it can control human actions (or that human will also choose the optimal action for the global system)

- Collaborative but approximately optimal
    - Same reward functions known by the agents
    - Assume people are greedy and take the action that maximizes direct reward (not the long-term accumulated reward)
    - Robot chooses action such that when coupled with greedy human action
    - Robot doesn't assume it can control the human but account for how the human will react to its action

- Non-Collaborative but computing best response to the robot
    - Assuming different reward functions (known by the agents)
    - Not solving the 2 player game (Nash equilibrium)
    - Assume human will take robot behavior as fixed and compute the best action sequence to take (according to its reward)
    - Robot computes action sequence that maximizes its reward taking the human response defined above into account
    - Re-planning actions at each time-step creates a reactive robot that has a model of how the person will respond to its actions as opposed to computing a joint global plan


### Human Behavior to infer Human internal state

- In reality Robot does not know the human reward function
    - Should adapt its own reward function for whatever the human wants
    - Possible to estimate the robot reward function from human action

- Assumption:
    - Human knows robot's reward
    - Human computes best response to the robot and have access to the full robot trajectory

- On-line inference by integrating over futures
    - infer the human reward function from observation of the human actions
    - use this estimated reward to change its plan / adapt to what human wants

- Active On-line Inference using Robot Physical Actions
    - Robot action influence human action
    - Leverage this influence to gather information about human reward

- What if the Human knows the robot is learning
    - Human might act differently in order to maximize the information the robot can learn from observation

### Human Inference about the robot
- Robot does not know everything about human
- Human does not know everything about robot

- Humans expect robot behavior to be approximately rational
    - Human estimates the robot reward function based on what they have observed sofar
    - Robot can estimate the reward function the human has inferred sofar
    - Robot can use this to plan actions accordingly in order to be predictable

- Humans Using Robot Behavior to Infer Robot Internal State
    - Human use robot model to do inference on robot internal states

 - Communicating Robot Goals
    - Robot purposefully deviates from efficiency in order to clarify its intentions

 - Communicating Robot reward parameters
    - Reward more and more complex
    - Modeling human inference about reward parameters
    - Enables robot to choose action sequences that communicates its reward function

## Conclusion
- Sort of review / overview of prior work
- Very interesting to see the evolution of the work
- Modeling interaction between agents is key to multi-agent cooperation

## Bibtex
```
@article{DBLP:journals/corr/Dragan17,
  author    = {Anca D. Dragan},
  title     = {Robot Planning with Mathematical Models of Human State and Action},
  journal   = {CoRR},
  volume    = {abs/1705.04226},
  year      = {2017},
  url       = {http://arxiv.org/abs/1705.04226},
  archivePrefix = {arXiv},
  eprint    = {1705.04226},
  timestamp = {Wed, 07 Jun 2017 14:43:08 +0200},
  biburl    = {https://dblp.org/rec/bib/journals/corr/Dragan17},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}
```