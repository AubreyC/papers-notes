# [Information gathering actions over human internal state](https://people.eecs.berkeley.edu/~dsadigh/Papers/sadigh-iros2016.pdf)

Uses the results from "Planning for autonomous cars that leverage effects on human actions" (D. Sadigh 2016)

## Context
- State estimation
    - e.g: driving style (aggressive, etc)

- How to get information on human internal state

- Interaction between robots and humans:
    - robot's actions can influence the human behavior / actions

## Problem

- Estimation of internal state is usually done passively
    - observe actions and states
    - estimate internal state from it

- Not very efficient and can be slow

## Idea / Solution / Method Proposed

- Leverage the fact that robot's actions affect human behavior
    - plan actions that would probe a reaction from the human
    - reaction such that it brings information on the human internal state

- Robots uses a reward function that combines:
    - reward to maximize information gathering
    - robot's actual reward function (avoid collisions, etc)

- Interaction between human and autonomous car modeled as a dynamical system (D. Sadigh 2016):
    - autonomous cars maximize a reward
    - humans maximize a reward function learned by using Inverse Reinforcement Learning (IRL)

## Limitations
- Need to find a good and safe way to balance between exploring (gathering information) and actually maximizing its own reward
- Not efficient right now
- Not clear how to scale up to more than two vehicles

## Conclusion
- It allows the robot to learn / adapt to different external agents by taking advantage of interaction

- Limited to simple situations:
    - well defined roads / intersections
    - only two agents: autonomous and human car

## Bibtex
```
@INPROCEEDINGS{7759036,
author={D. Sadigh and S. S. Sastry and S. A. Seshia and A. Dragan},
booktitle={2016 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS)},
title={Information gathering actions over human internal state},
year={2016},
volume={},
number={},
pages={66-73},
keywords={human-robot interaction;learning systems;path planning;state estimation;action planning;active information gathering;autonomous car;driving style;human action observation;human driver lane;human internal state estimation;passive state estimation;robot action;Autonomous automobiles;Computational modeling;Probes;State estimation;Vehicles},
doi={10.1109/IROS.2016.7759036},
ISSN={},
month={Oct},}
```