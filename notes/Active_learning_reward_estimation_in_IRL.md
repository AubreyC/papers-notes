# [Active Learning for Reward Estimation in Inverse Reinforcement Learning](https://flowers.inria.fr/mlopes/myrefs/09-ecml-airl.pdf)

## Context

- Inverse Reinforcement Learning: Recovering the reward function of a Markov Decision Process from an expert

- Active Learning:
    - initial set of expert demonstration
    - ability to query the expert for specific example to optimize the learning process

- Goal is to reduce the number of demonstrations needed: Active learning
    - Possibility for the learner to query the expert
    - Actively selecting potentially informative samples

## Problem

- IRL is hard:
    - requires a lot of demonstrations
    - not sure the demonstrations brings information

- IRL is ill posed problem
    - several reward functions can correspond to the same optimal policy

## Idea / Solution / Method Proposed

- Bayesian formualtion:
    - IRL is cast as an inference problem
    - Posterior probability distribution of `r` takes into account demonstrations and prior distribution of r

- Methods:
    - Gradient based method for IRL (see paper for details)
    - Monte Carlo Markov Chain method (see paper for details)

- Active sampling:
    - Initial set of demonstrations
    - Finding states with highest uncertainty on the policy learned (defined by the learned/estimated reward function)
    - Query demonstration for states with highest uncertainty on the policy (maximizing the entropy)

## Limitation / Edge Cases

- Not usefull on non-parametrized reward functions and state-wise independant reward function:
    - not enough structure to generalize learned policy on observed states to non-obesvered states
    - reward function has enough degrees of freedom to yield any policy

## Conclusion

- Active learning: idea of maximizing the information of the samples used is very interesting as it helps reducing the number of samples needed
- Used Gradient based IRL methods
- Works on benchmark examples

## Bibtex
```
@inproceedings{Lopes:2009:ALR:1617459.1617463,
 author = {Lopes, Manuel and Melo, Francisco and Montesano, Luis},
 title = {Active Learning for Reward Estimation in Inverse Reinforcement Learning},
 booktitle = {Proceedings of the European Conference on Machine Learning and Knowledge Discovery in Databases: Part II},
 series = {ECML PKDD '09},
 year = {2009},
 isbn = {978-3-642-04173-0},
 location = {Bled, Slovenia},
 pages = {31--46},
 numpages = {16},
 url = {http://dx.doi.org/10.1007/978-3-642-04174-7_3},
 doi = {10.1007/978-3-642-04174-7_3},
 acmid = {1617463},
 publisher = {Springer-Verlag},
 address = {Berlin, Heidelberg},
}

```