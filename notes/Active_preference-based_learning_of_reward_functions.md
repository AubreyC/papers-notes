# [Active Preference-Based Learning of Reward Functions](http://rss2017.lids.mit.edu/program/papers/04/)

Uses the results from "Planning for autonomous cars that leverage effects on human actions" (D. Sadigh 2016)
Learning reward function by showing preference over trajectories.
Interesting but not crazy.

## Context

- Human drivers are driven by a reward function
- Learning reward function

## Problem

- Reward function hard to handcraft
- IRL techniques require demonstrations
    - demonstrations can be hard to obtain
    - demonstrations might not display dangerous behavior

## Idea / Solution / Method Proposed

- Preference based reward function learning, iteratively:
    - show two trajectories to human
    - human need to choose which one is the "best"

- Active trajectories generation:
    - two trajectories are generated (so the human has to choose between these two)
    - maximizes the information this would bring (optimization problem using sampled based approach)

- Learning the reward function (weights w):
    - given the preference over two trajectories
    - w is updated using Bayesian inference

## Limitations

- Human reward is assumed to be a linear combination of features
- Local optimization

## Conclusion
- Idea that preferences over some trajectories brings informations about the underlying reward is interesting
- Not sure how this could be adapted to more complex reward function (non-linear)

## Bibtex
```
@article{DBLP:journals/corr/abs-1208-0984,
  author    = {Riad Akrour and
               Marc Schoenauer and
               Mich{\`{e}}le Sebag},
  title     = {{APRIL:} Active Preference-learning based Reinforcement Learning},
  journal   = {CoRR},
  volume    = {abs/1208.0984},
  year      = {2012},
  url       = {http://arxiv.org/abs/1208.0984},
  archivePrefix = {arXiv},
  eprint    = {1208.0984},
  timestamp = {Wed, 07 Jun 2017 14:42:46 +0200},
  biburl    = {http://dblp.org/rec/bib/journals/corr/abs-1208-0984},
  bibsource = {dblp computer science bibliography, http://dblp.org}
}
```