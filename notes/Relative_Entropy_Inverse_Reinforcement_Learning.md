# [Relative Entropy Inverse Reinforcement learning](http://proceedings.mlr.press/v15/boularias11a.html)

## Context
- Imitation Learning / Learning from Demonstration (LfD)
    - For complex task, usually easier to demonstrate the task than engineering a good reward function for it

- Imitation Learning:
    - Learn a direct mapping from state to action
    - Supervised learning, but does not capture the influence of current action on future reward

- Inverse Reinforcement Learning:
    - From demonstrations, Recover the reward function under which the expert policy is optimal

- Demonstrations from expert:
    - Only a covering a small part of the whole state space

## Problem
- Many IRL methods assume:
    - Model of the MDP is given or can be accurately learn from demonstrated trajectories
    - Usually need a sub-routine to solve the forward MDP problem in their iteration loop (Find optimal policy from a given reward)

- Maximum Entropy approach: Policy that best represents the demonstrated behavior
    - Highest entropy policy
    - Subject to the constraint of matching the reward value of demonstrated actions (match features expectation)
    - Require to know the transition function => New method to solve this without the transition function

## Idea / Solution / Method Proposed
- Model Free IRL algorithm

- Build on:
    - Maximum Entropy Inverse Reinforcement Learning (B. Ziebart, 2008)
    - Relative Entropy Policy Search (J. Peters 2010)
    - Proposes a method to solves the Maximum Entropy IRL problem without transition function

- Minimize the Relative Entropy (KL Divergence) between :
    - Empirical distribution of the state-action trajectories under baseline policy
    - Learned Policy
    - With a constraint that the learned policy matches the reward feature count of the demonstrations

- Require to compute a Gradient
    - Proposes a sampled based gradient estimation using Importance sampling

## Limitation / Edge Cases
- Reward as a linear function of features

## Conclusion
- Sampled based method for IRL
- Model free, very interesting for real-life application

## Summary

In the context of Learning from Demonstration, Inverse Reinforcement Learning is a very powerful tool as the reward function is considered to be the most robust, generalizable and succinct formulation of how to complete a specific task. However, many IRL algorithms require to solve the forward Reinforcement Learning problem in their iteration loop, relying on a sub-routine to do so, which can be inaccurate and very computationally expensive.

The Maximum Entropy Inverse Reinforcement Learning (B. Ziebart, 2008) formulates this problem as finding the policy with the highest entropy while matching the reward value of the demonstrated actions (matching the feature expectation). The solution to find this policy assume that the transition matrix (dynamic of the MDP) is known. In this article, the author proposes to reformulate the Maximum Entropy IRL problem using the idea developed in Relative Entropy Policy Search (J. Peters, 2010). This re-formulation enable to solve this problem without the need of the transition matrix by using a sampled based gradient estimation method.

Interesting method as it allows to solve IRL problem when the dynamic is unknown. However, as in many IRL algorithms, the reward function is considered to be a linear function of hand-crafted features.

## Bibtex

```
@InProceedings{pmlr-v15-boularias11a,
  title =    {Relative Entropy Inverse Reinforcement Learning},
  author =   {Abdeslam Boularias and Jens Kober and Jan Peters},
  booktitle =    {Proceedings of the Fourteenth International Conference on Artificial Intelligence and Statistics},
  pages =    {182--189},
  year =     {2011},
  editor =   {Geoffrey Gordon and David Dunson and Miroslav Dudík},
  volume =   {15},
  series =   {Proceedings of Machine Learning Research},
  address =      {Fort Lauderdale, FL, USA},
  month =    {11--13 Apr},
  publisher =    {PMLR},
  pdf =      {http://proceedings.mlr.press/v15/boularias11a/boularias11a.pdf},
  url =      {http://proceedings.mlr.press/v15/boularias11a.html},
  abstract =     {We consider the problem of imitation learning where the examples, demonstrated by an expert, cover only a small part of a large state space. Inverse Reinforcement Learning (IRL) provides an efficient tool for generalizing the demonstration, based on the assumption that the expert is optimally acting in a Markov Decision Process (MDP). Most of the past work on IRL requires that a (near)-optimal policy can be computed for different reward functions. However, this requirement can hardly be satisfied in systems with a large, or continuous, state space. In this paper, we propose a model-free IRL algorithm, where the relative entropy between the empirical distribution of the state-action trajectories under a baseline policy and their distribution under the learned policy is minimized by stochastic gradient descent. We compare this new approach to well-known IRL algorithms using learned MDP models. Empirical results on simulated car racing, gridworld and ball-in-a-cup problems show that our approach is able to learn good policies from a small number of demonstrations.  [pdf]}
}
```