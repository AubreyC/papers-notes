# [Planning-Based Prediction for Pedestrians](http://ieeexplore.ieee.org/abstract/document/5354147/)

Application of Maximum Entropy Inverse reinforcement learning method pedestrians motion predictions, and use these predictions for motion planning.

## Context
- Robot motion planning:
    - In human environment
    - Without hindering human

## Problem
- Common approach:
    - Kalman filter used as prediction algorithms: not good for long time horizon
    - Hidden Markov models: does not generalize well

## Idea / Solution / Method Proposed
- Assume people behave like planner: optimizing a cost function

- Using Maximum Entropy Inverse Reinforcement Learning:
    - soft-maximum version of Markov Decision Process
    - account for decision uncertainty
    - learn the cost weights (cost function linear combination of features)

- Algorithm:
    - Cost function learning: MaxEnt IRL
    - Future Trajectory Prediction:
        - Destination estimation: Bayesian estimation of pedestrian destination (goal)
        - State expected visitation count: Soft value iteration algorithm
    - Motion planner: Predictive planner that takes future human trajectory into account

## Limitation / Edge Cases
- Cost function:
    - Linear combination of hand-crafted feature function
- Time-variation handles as a "hack"
- Interaction between robot and human are neglected
- No notion of velocity of the pedestrian

## Conclusion
- Application on real problem
- Full application: Learning + Motion prediction + Motion planning
- However:
    - simple cost (linear)
    - Interaction between robot and human ignored
    - No velocity for human (only occupation grid)

## Bibtex
```
@INPROCEEDINGS{5354147,
author={B. D. Ziebart and N. Ratliff and G. Gallagher and C. Mertz and K. Peterson and J. A. Bagnell and M. Hebert and A. K. Dey and S. Srinivasa},
booktitle={2009 IEEE/RSJ International Conference on Intelligent Robots and Systems},
title={Planning-based prediction for pedestrians},
year={2009},
volume={},
number={},
pages={3931-3936},
keywords={maximum entropy methods;mobile robots;optimal control;path planning;cost function;goal-directed trajectories;hindrance-sensitive robot trajectory planning;incremental planner;maximum entropy inverse optimal control;pedestrians;planning based prediction;robot movements;Computer science;Cost function;Entropy;Hidden Markov models;Intelligent robots;Optimal control;Predictive models;Trajectory;USA Councils;Uncertainty},
doi={10.1109/IROS.2009.5354147},
ISSN={2153-0858},
month={Oct},}
```