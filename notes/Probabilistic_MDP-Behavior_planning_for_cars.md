# [Probabilistic MDP-Behavior Planning for cars](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=6082928)

## Context
- Behavior Planning with Markov Decision Process
- Include other agents prediction / interactions

## Problem
- Situations very diverse
- High interaction between vehicles
- High uncertainty:
    - Occlusion in the perception
    - Sensors noise / uncertainty

## Idea / Solution / Method Proposed

- Formulate driving task as MDP
    - State: parametrization of every vehicles (s_ego, s_1, s_2, ...)
    - Action: Only for ego vehicle
    - Transition model: Captures uncertainty and interaction with surrounding agents

- Transition Probability:
    - Derived from hierarchical Dynamic Bayesian Network (DBN) formulation from "A probabilistic model for estimating driver behaviors and vehicle trajectories in traffic environments" (T. Gindel, 2010)
    - Particle based: Sample ego-vehicle actions and propagate through DBN and generate transition probabilities

- Reward model:
    - Total reward computed by integrating continuous reward over time

## Limitation / Edge Cases
- DBN definition in another article, but exact definition of DBN not clear
- MDP models only the ego-vehicle:
    - other agents are modeled according to DBN only
    - not sure how they take decisions

## Conclusion
- A little bit confusing what is the point of using MDP here
- Other agents are modeled with DBN, but not very clear
- That means interaction is considered only in one way: behavior of other agents totally captured in DBN

## Bibtex
```
@INPROCEEDINGS{6082928,
author={S. Brechtel and T. Gindele and R. Dillmann},
booktitle={2011 14th International IEEE Conference on Intelligent Transportation Systems (ITSC)},
title={Probabilistic MDP-behavior planning for cars},
year={2011},
volume={},
number={},
pages={1537-1542},
keywords={Bayes methods;Markov processes;automobiles;decision making;decision theory;Markov decision process;abstract symbolic states;cars;complex continuous temporal models;dynamic Bayesian networks;high-level decision making;mathematical framework;probabilistic MDP-behavior planning;probability theory;random variables;traffic environments;Acceleration;Decision making;Optimization;Planning;Probabilistic logic;Safety;Vehicles},
doi={10.1109/ITSC.2011.6082928},
ISSN={2153-0009},
month={Oct},}
```