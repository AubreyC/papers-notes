# [Imitation Learning for Locomotion and Manipulation](https://www.ri.cmu.edu/pub_files/pub4/ratliff_nathan_2007_4/ratliff_nathan_2007_4.pdf)

Paper that presents experimental results of applying functional gradient technique for optimizing a structured margin formulation of the corresponding large non-linear multi-class classification problems.

## Context
- Imitation Learning: Learning from Demonstration
- Decision making in robotics:
    - Choose optimal action for a given state
    - Seen as a multi-class classification problem: Actions seen as a labels for states

- Apply recent advance in multi-class learning for robotic
    - Detailed on how to apply this on grasping and quadruple locomotion example.

## Problem
- Hard to manually engineer a cost function to encode specific action
- Easier to demonstrate it

## Idea / Solution / Method Proposed
- Multi-class classification
    - Action are selected by taking the action with the maximum score
    - Can be seen as Q function approximation, with greedy policy according to this Q function

- Optimize a convex upper bound on a loss function
    - Loss function: associated on loss of choosing wrong label (action)
    - Use structured-margin loss function
    - Improve generalization

- Functional gradient optimization
    - On the direction in the space of score function that would improve the performances on the loss function
    - Project gradient on a functional space (here NN function)

## Limitation / Edge Cases
- Discretization of the state/action space
- In the example: Only a "one-step ahead" greedy approach is considered, but a full planning based solution could be apply
- This can be seen as learning a Q function from examples, but what guarantee?
- Does not seem to capture the dynamic of the process: more like a blind approach of just repeating what has been seen

## Conclusion
- Approach of using multi-class classifiers and gradient based optimization technique for control tasks
- Good results on real-world application: grasping and locomotion
- Rely on discretization, and does not seem to capture the dynamic of the system

## Summary

Robotics involves a lot of decision making. For a given state, an action is chosen in order to accomplish a specific task (e.g usually optimal with respect to a cost function). However, it is often easier to demonstrate the task than to manually craft the cost function that would enable the system to perform the task. This process is referred as Imitation Learning, or Learning from Demonstration.

This problem can often be formulated as a classification problem where for any given states the actions are the labels. This papers applies recent advance in multi-class learning technique to solve problems in robotic grasping and quadruple locomotion. From demonstration, a score function is learned, and then actions are selected according to the maximization of this score function (greedy policy). To learn the score function, a convex bound of a loss function is optimized, which allow to improve generalization by introducing a notion of structured margin. Score function are optimized in a functional space (here a Neural Network function space) by projecting the loss function gradient on this space.

Useful approach as it uses recent advance in multi-class classifier for imitation learning. It shows pretty good results on the grasping and quadruple locomotion problems. This method can be seen a learning a Q function as a NN function from demonstration, and then defining the policy as the greedy policy that take the action that maximize this Q function.
However, it looks like this method does not capture the dynamics of the system at all, and just tries to "blindly" reproduce what it has seen in the example.

## Bibtex

```
@INPROCEEDINGS{4813899,
author={N. Ratliff and J. A. Bagnell and S. S. Srinivasa},
booktitle={2007 7th IEEE-RAS International Conference on Humanoid Robots},
title={Imitation learning for locomotion and manipulation},
year={2007},
volume={},
number={},
pages={392-397},
keywords={intelligent robots;legged locomotion;manipulators;decision making problems;footstep prediction;imitation learning;multiclass classification framework;quadruped locomotion;robotic hand;terrain height map;Actuators;Decision making;Design optimization;Humans;Machine learning;Nonlinear dynamical systems;Orbital robotics;Robots;Stability;Testing},
doi={10.1109/ICHR.2007.4813899},
ISSN={2164-0572},
month={Nov},}
```

