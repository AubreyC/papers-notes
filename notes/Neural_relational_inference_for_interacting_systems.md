# [Neural Relational Inference for Interacting Systems](https://arxiv.org/pdf/1802.04687.pdf)

Prediction of future states for dynamics systems with interaction. This paper proposes a model that as a first step estimate the graph structure of the system (interaction between objects), and then uses this structure to predict next states of the system. It uses the theory of Graph Neural Networks.

Related to:
- Neural Message Passing for Quantum Chemistry (J. Gilmer, 2017) [[paper](https://arxiv.org/abs/1704.01212)]
- Interaction Networks for Learning about Objects, Relations and Physics (P. Battaglia, 2016) [[paper](https://arxiv.org/abs/1612.00222)]

## Context

- Interacting / Coupled systems are everywhere

- Need to understand the system better in order to predict its evolution

## Problem

- Interplay between components / object
    - highly complex behavior
    - hard to explain / understand / predict without understanding the underlying interaction between the objects

- But in reality
    - simple model of the objects
    - simple interactions model
    - highly complex behavior

## Idea / Solution / Method Proposed

- Introduce Neural Relational Inference model:
    - based on Graph Neural Network models
    - variational autoencoder:
        - Encoder: estimate egde types from trajectories of objects (type of link between the objects)
        - Decoder: from edge type and trajectroies up to the current time


- In the case of particles attached with springs 2 by 2:
    - node: each particle is a node
    - edge: spring / link is an edge

## Limitations

- Limited to static graphs during training
    - lots of real worl problem involves dynamic interaction evolving with time
    - future work is to investigate how to model dynamical latent interactions

## Conclusion

- Understanding the underlying interactions allows to better understand and model complex systems
- very interesting to understand traffic behavior where interaction is omnipresent
- Good results on examples

## Bibtex
```
@ARTICLE{2018arXiv180204687K,
   author = {{Kipf}, T. and {Fetaya}, E. and {Wang}, K.-C. and {Welling}, M. and
    {Zemel}, R.},
    title = "{Neural Relational Inference for Interacting Systems}",
  journal = {ArXiv e-prints},
archivePrefix = "arXiv",
   eprint = {1802.04687},
 primaryClass = "stat.ML",
 keywords = {Statistics - Machine Learning, Computer Science - Learning},
     year = 2018,
    month = feb,
   adsurl = {http://adsabs.harvard.edu/abs/2018arXiv180204687K},
  adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
```