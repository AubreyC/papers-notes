# [Observe and Look Further: Achieving Consistent Performance on Atari](https://arxiv.org/abs/1805.11593)

Deep Reinforcement Learning using DQN. Improvements such as processing un-altered reward of different scale and density (transformed Bellman operator), higher discount reward factor enabling to take into account rewarding states that are further away (Temporal Consistency loss) and more efficient exploration (using expert demonstrations in the learning process).

Related to:

- Human-level control through deep reinforcement learning (Deep Mind, 2015) [[paper](https://storage.googleapis.com/deepmind-media/dqn/DQNNaturePaper.pdf)]
- Deep Q-Learning from Demonstrations (Deep Mind, 2017) [[paper](https://arxiv.org/abs/1704.03732)]


## Context

- Deep Reinforcement Learning
    - high performance and complicated task
    - no model of the dynamics (just interaction with the model)

- Use a Deep Network to estimate the optimal Q-function (action value function)
    - optimal policy is the greedy policy for this Q-function

- DQN classical algorithm:
    - Generate samples of transition from e-greedy policy of the current Q_t function
    - Step of the  difference between Q_(t+1) function (deep net parametrized by theta) and T\*Q_t (T Bellman operator) (computed at the sample states)


## Problem

- Processing diverse rewards distribution:
    - Algorithm can handle diverse kind of reward (amplitude, distribution, etc)
    - In previous work, reward are clipped to -1,0,1 preventing to use nuances in reward (states more rewarding than other, etc)
    - clipping reward changes the optimal policy

- Reason over long time horizon
    - High rewarding state in long time horizon

- Efficient learning
    - Less step to find a good policy (or action-value function / Q function)

## Idea / Solution / Method Proposed

- Processing diverse rewards distribution: Transformed Bellman Operator
    - introduces Transformed Bellman Operator
    - enables to reduce the scale of the action-value function, effectively reducing its variance
    - avoid divergence of the optimization algorithm (find the network parameters to approximate the Q function)

- Reason over long time horizon: Temporal Consistency loss
    - Problem
        - Increasing discount factor: decrease temporal difference in value between non-rewarding state
        - Unwanted generalization of the neural network (Q function) to the next state, due to the similarity
of temporally adjacent target values
    - Solution
        - Add a loss to prevent generalization on un-visited state
        - Penalizes weight update that change the next action-value estimate (not in the set of sample transition)

- Efficient learning: Supervised Imitation Loss
    - for transition that are in the expert replay buffer
    - loss that aims at setting high the action-value function for same action as the one from the expert (with a margin)

## Limitations

- Expert demonstration used in a very Imitation Learning manner
- This assumes that the expert demonstrations are indeed optimal for the specified reward function (or at least very good)
- Why using a fixed ratio between expert transition and actor transition?
    - What if the expert demonstration is not quite optimal optimal for the specified reward?

## Conclusion

- Expert demonstrations are taken into account to guide the estimation of the optimal Q function
- Increased stability of the learning process enabling higher discount factor

## Bibtex
```
@ARTICLE{2018arXiv180511593P,
   author = {{Pohlen}, T. and {Piot}, B. and {Hester}, T. and {Gheshlaghi Azar}, M. and
    {Horgan}, D. and {Budden}, D. and {Barth-Maron}, G. and {van Hasselt}, H. and
    {Quan}, J. and {Ve{\v c}er{\'{\i}}k}, M. and {Hessel}, M. and
    {Munos}, R. and {Pietquin}, O.},
    title = "{Observe and Look Further: Achieving Consistent Performance on Atari}",
  journal = {ArXiv e-prints},
archivePrefix = "arXiv",
   eprint = {1805.11593},
 primaryClass = "cs.LG",
 keywords = {Computer Science - Learning, Computer Science - Artificial Intelligence, Statistics - Machine Learning},
     year = 2018,
    month = may,
   adsurl = {http://adsabs.harvard.edu/abs/2018arXiv180511593P},
  adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}

```