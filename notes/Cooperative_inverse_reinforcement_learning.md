# [Cooperative Inverse Reinforcement Learning](https://arxiv.org/abs/1606.03137)

Problem with IRL, people might not act optimally with respect to the reward function when they
know that the robot is trying to learn something from them. This articles introduces learning algorithm that induces active learning / teaching behavior.

## Context
- Value alignment:
    - robot should do what the human would like it to do
    - robot should try to help human achieve their goal as opposed to try to achieve the same goal but for itself

- Learning from Demonstration
    - Cooperative learning with Human and Robot actively optimize the learning process

- Cooperative environment
    - Environment is shared between the robot and the human

## Problem
- Mis-specifying objective function can result in catastrophic behavior

- Inverse Reinforcement Learning: Learning Reward function from demonstration has problems:
    - we want to robot to help the human optimizing its reward function, not optimizing it as its own
    - human behavior might not be optimal when teaching

## Idea / Solution / Method Proposed

- Important field for this work:
    - Inverse Reinforcement Learning: infer objective / reward function from demonstrations
    - Optimal Teaching: behavior that will optimize learning for the other agent
    - Principal-agent model: study the how one agent's actions influence the other agent in non-cooperative settings

- Formulation: Cooperative Inverse Reinforcement Learning
    - MDP with human and robot
    - human and robot have the same joint reward function (not just identical but actually the same)
    - Human and Robot have joint reward function: incentive for the human to teach the robot

- Results:
    - Structural Results: CIRL can be reduced to solving a POMDP
    - Apprenticeship learning is subclass of CIRL

- Generating Instructive Demonstrations
    - Computing the Human best response (behavior) when the robot is learning reward with IRL
    - Generate behavior that optimize learning for the robot, and in return enable to optimize the joint R since the robot gets better

## Limitation / Edge Cases
- Work on toy examples
- Linear reward function

## Conclusion
- Formulation of the cooperative system with shared reward function
- Cooperative teaching behavior improve global gain

## Bibtex
```
@article{DBLP:journals/corr/Hadfield-Menell16,
  author    = {Dylan Hadfield{-}Menell and
               Anca D. Dragan and
               Pieter Abbeel and
               Stuart J. Russell},
  title     = {Cooperative Inverse Reinforcement Learning},
  journal   = {CoRR},
  volume    = {abs/1606.03137},
  year      = {2016},
  url       = {http://arxiv.org/abs/1606.03137},
  archivePrefix = {arXiv},
  eprint    = {1606.03137},
  timestamp = {Wed, 07 Jun 2017 14:40:43 +0200},
  biburl    = {https://dblp.org/rec/bib/journals/corr/Hadfield-Menell16},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}
```