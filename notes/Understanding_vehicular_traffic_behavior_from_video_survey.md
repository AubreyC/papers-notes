# [Understanding vehicular traffic behavior from video: a survey of unsupervised approaches](https://pdfs.semanticscholar.org/e722/9ba896b4ab6f8cc44a7f8dbff4fb1335cc18.pdf)

Survey paper on unsupervised approaches to traffic behavior modeling from video, focusing on trajectory clustering and topic modeling.

## Context
- Lots of camera: usually only used after-the-fact
- Learn from camera footage
    - motion pattern
    - behavior

## Problem
- Detection and tracking system is challenging
    - under some conditions (weather, etc)
    - in densely occupied scenes

- Predicting future state is challenging
    - intention
    - interaction
    - decision

## Idea / Solution / Method Proposed
- Observing motion over time
    - typical motion patterns can be learned and used as a priori knowledge for prediction.

- Learning Behavior with Trajectories
    - Trajectories extracted from video
        - Grouped in clusters which represent prototypical patterns of motion
        - Resulting models: description the scene and typical behaviors
    - Challenges with some proposed methods from prior work:
        - How to compare trajectories?
        - How to model/parametrize the clusters?

- Learning Behavior with Topic Models
    - Topic Model: recognize relationships through the co-occurrence of simple features
        - e.g in NLP: words associated to a topic because often appear in this topic
    - Different methods presented

- List of popular dataset is given

## Limitation / Edge Cases
- Training is done on one situation and does not transfer well
- Methods are not really oriented toward high level behavior understanding

## Conclusion
- Interesting for trajectory clustering / defining paths
- Not really high level understanding

## Bibtex
```
@article{DBLP:journals/jei/MorrisT13,
  author    = {Brendan Tran Morris and
               Mohan Manubhai Trivedi},
  title     = {Understanding vehicular traffic behavior from video: a survey of unsupervised
               approaches},
  journal   = {J. Electronic Imaging},
  volume    = {22},
  number    = {4},
  pages     = {041113},
  year      = {2013},
  url       = {https://doi.org/10.1117/1.JEI.22.4.041113},
  doi       = {10.1117/1.JEI.22.4.041113},
  timestamp = {Sun, 28 May 2017 13:22:39 +0200},
  biburl    = {https://dblp.org/rec/bib/journals/jei/MorrisT13},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}
```