# [Understanding human intentions via Hidden Markov Models in autonomous mobile robots](http://ieeexplore.ieee.org/document/6249459/)

Activity encoded as HMMS, with intention/goals of one activity as hidden state in the corresponding HMM. Training (learning transition probabilities) using Baum-Welch algorithms and recognition using maximum likelihood HMM and Viterbi algorithm to determine the most probable hidden states (intentions).

## Context
- Understanding Human Intentions
- Intentions allow for prediction and interpretation of each other's actions
- Hidden Markov Models

## Problem
- Understanding Intent:
    - Crucial aspect of multi-agent system

- How to model activity
- How to recognize the activity

## Idea / Solution / Method Proposed

- Modeling:
    - Activity represented by one HMM per activity: e.g Meeting, Passing by, etc
    - Activity is composed by intention/goal (hidden state) that changes over time: e.g Facing each other at 1m
    - Visible variables: change in features: e.g distance decreasing, angle increasing, etc

- Two stages:
    - Activity modeling
    - Intent recognition

- Activity modeling:
    - One HMM for each activity
    - Visible variables:
        - Encoded with features
        - Changes in features (e.g distance decrease between agent decrease, etc)
    - For each activity:
        - activity are performed by the robot itself
        - transition and observation probabilities are learned from these demonstrations (Baum-Welch algorithm)

- Intent recognition:
    - Robot observe the situation from external point of view, and transform it to observation from first point of view (to match the training phase)
    - Activity Recognition: max likelihood of observing observation for each activity (HMMs) using Forward Algorithm
    - For highest likelihood activity: Find most probable sequence of hidden state which correspond to goals of the activity

## Limitations
- Manual design of features
- Being able to have the robot completing the activity
- Each activity is a full HMM, no notion of low-level dynamic of the system
- Doesn't account for sub-optimality / uncertainty of how to realize a task

## Conclusion
- Interesting as a first order approach
- Simple approach

## Bibtex
```
@INPROCEEDINGS{6249459,
author={R. Kelley and M. Nicolescu and A. Tavakkoli and M. Nicolescu and C. King and G. Bebis},
booktitle={2008 3rd ACM/IEEE International Conference on Human-Robot Interaction (HRI)},
title={Understanding human intentions via Hidden Markov Models in autonomous mobile robots},
year={2008},
volume={},
number={},
pages={367-374},
keywords={hidden Markov models;mobile robots;object detection;recursive estimation;robot vision;target tracking;autonomous mobile robots;hidden Markov models;human cognitive system;human intentions;nonparametric recursive modeling;sensory-motor capability;target detection;target tracking;vision-based technique;Computational modeling;Hidden Markov models;Humans;Robot kinematics;Robot sensing systems;Training;Hidden Markov Models;Human-robot interaction;Theory of Mind;intention modeling;vision-based methods},
doi={10.1145/1349822.1349870},
ISSN={2167-2121},
month={March},}
```